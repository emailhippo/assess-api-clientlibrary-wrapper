﻿// <copyright file="DefaultClientTests.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Tests.Integration.Logic.Clients.V1
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using EmailHippo.Assess.Api.Client.Entities.Clients.V1;
    using EmailHippo.Assess.Api.Client.Entities.Configuration.V1;
    using EmailHippo.Assess.Api.Client.Interfaces.Configuration;
    using EmailHippo.Assess.Api.Client.Logic.Clients.V1;
    using EmailHippo.Assess.Api.Client.Tests;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using NUnit.Framework.Legacy;

    /// <summary>
    /// Default Client Tests.
    /// </summary>
    [TestFixture]

#if !DEBUG
    [Ignore("Integration Test. Do not run on build server.")]
#endif
    public sealed class DefaultClientTests : TestBase
    {
        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultClientTests"/> class.
        /// </summary>
        public DefaultClientTests()
        {
            this.logger = this.LoggerFactory.CreateLogger<DefaultClientTests>();
        }

        /// <summary>
        /// Processes the asynchronous when valid email expect valid result.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("abuse@hotmail.com")]
        [TestCase("abuse@yahoo.com")]
        [TestCase("syntax@syntax.com")]
        public async Task ProcessAsync_WhenValidEmail_ExpectValidResult([NotNull] string email)
        {
            // Arrange
            var mockConfig = new Mock<IConfiguration<KeyAuthentication>>();

            mockConfig.Setup(r => r.Get).Returns(() => new KeyAuthentication { LicenseKey = LicenseKey });

            var defaultClient = new DefaultClient(this.LoggerFactory, mockConfig.Object);

            // Act
            var stopwatch = Stopwatch.StartNew();
            var result = await defaultClient.ProcessAsync(new VerificationRequest { Email = email }, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            ClassicAssert.True(result.Result != null);
            this.logger.LogInformation("Result:{0}", JsonConvert.SerializeObject(result));
            Console.WriteLine("Result:{0}", JsonConvert.SerializeObject(result));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Processes the asynchronous when valid email first name expect valid result.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="firstName">The first name.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("abuse@hotmail.com", "John")]
        [TestCase("abuse@yahoo.com", "Jane")]
        [TestCase("syntax@syntax.com", "Roger")]
        public async Task ProcessAsync_WhenValidEmailFirstName_ExpectValidResult([NotNull] string email, string firstName)
        {
            // Arrange
            var mockConfig = new Mock<IConfiguration<KeyAuthentication>>();

            mockConfig.Setup(r => r.Get).Returns(() => new KeyAuthentication { LicenseKey = LicenseKey });

            var defaultClient = new DefaultClient(this.LoggerFactory, mockConfig.Object);

            // Act
            var stopwatch = Stopwatch.StartNew();
            var result = await defaultClient.ProcessAsync(new VerificationRequest { Email = email, FirstName = firstName }, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            ClassicAssert.True(result.Result != null);
            this.logger.LogInformation("Result:{0}", JsonConvert.SerializeObject(result));
            Console.WriteLine("Result:{0}", JsonConvert.SerializeObject(result));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Processes the asynchronous when valid email last name expect valid result.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="lastName">The last name.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("abuse@hotmail.com", "Smith")]
        [TestCase("abuse@yahoo.com", "Wilkinson")]
        [TestCase("syntax@syntax.com", "Doe")]
        public async Task ProcessAsync_WhenValidEmailLastName_ExpectValidResult([NotNull] string email, string lastName)
        {
            // Arrange
            var mockConfig = new Mock<IConfiguration<KeyAuthentication>>();

            mockConfig.Setup(r => r.Get).Returns(() => new KeyAuthentication { LicenseKey = LicenseKey });

            var defaultClient = new DefaultClient(this.LoggerFactory, mockConfig.Object);

            // Act
            var stopwatch = Stopwatch.StartNew();
            var result = await defaultClient.ProcessAsync(new VerificationRequest { Email = email, LastName = lastName }, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            ClassicAssert.True(result.Result != null);
            this.logger.LogInformation("Result:{0}", JsonConvert.SerializeObject(result));
            Console.WriteLine("Result:{0}", JsonConvert.SerializeObject(result));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Processes the asynchronous when valid email last name expect valid result.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("abuse@hotmail.com", "127.0.0.1")]
        [TestCase("abuse@yahoo.com", "127.0.0.1")]
        [TestCase("syntax@syntax.com", "127.0.0.1")]
        public async Task ProcessAsync_WhenValidEmailIpAddress_ExpectValidResult([NotNull] string email, string ipAddress)
        {
            // Arrange
            var mockConfig = new Mock<IConfiguration<KeyAuthentication>>();

            mockConfig.Setup(r => r.Get).Returns(() => new KeyAuthentication { LicenseKey = LicenseKey });

            var defaultClient = new DefaultClient(this.LoggerFactory, mockConfig.Object);

            // Act
            var stopwatch = Stopwatch.StartNew();
            var result = await defaultClient.ProcessAsync(new VerificationRequest { Email = email, IpAddress = ipAddress }, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            ClassicAssert.True(result.Result != null);
            this.logger.LogInformation("Result:{0}", JsonConvert.SerializeObject(result));
            Console.WriteLine("Result:{0}", JsonConvert.SerializeObject(result));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}