﻿// <copyright file="ApiClientFactoryV1Tests.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Tests.Integration.Service
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using EmailHippo.Assess.Api.Client.Entities.Service.V1;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using NUnit.Framework.Legacy;

    /// <summary>
    /// Api Client Factory V3 Tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    [TestFixture]

#if !DEBUG
    [Ignore("Integration Test. Do not run on build server.")]
#endif
    public sealed class ApiClientFactoryV1Tests : TestBase
    {
        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiClientFactoryV1Tests"/> class.
        /// </summary>
        public ApiClientFactoryV1Tests()
        {
            this.logger = this.LoggerFactory.CreateLogger<ApiClientFactoryV1Tests>();

            ApiClientFactory.Initialize(LicenseKey, this.LoggerFactory);
        }

        [ItemNotNull]
        [NotNull]
        private static IEnumerable<VerificationDataRequest> TestList1 => new List<VerificationDataRequest>
        {
            new VerificationDataRequest { EmailAddress = "abuse@hotmail.com" },
            new VerificationDataRequest { EmailAddress = "abuse@aol.com" },
            new VerificationDataRequest { EmailAddress = "abuse@yahoo.com" },
            new VerificationDataRequest { EmailAddress = "abuse@bbc.co.uk" },
            new VerificationDataRequest { EmailAddress = "abuse@mailinator.com" },
            new VerificationDataRequest { EmailAddress = "abuse@abc.com" },
            new VerificationDataRequest { EmailAddress = "abuse@microsoft.com" },
            new VerificationDataRequest { EmailAddress = "abuse@gmail.com" },
        };

        /// <summary>
        /// Gets the performance test list1.
        /// </summary>
        /// <value>
        /// The performance test list1.
        /// </value>
        [NotNull]
        [ItemNotNull]
        private static IEnumerable<VerificationDataRequest> PerformanceTestList1
        {
            get
            {
                const int ReturnedItems = 10;
                const string DomainToTest = @"gmail.com";

                var rtn = new List<VerificationDataRequest>();

                for (int i = 0; i < ReturnedItems; i++)
                {
                    var randomFileName = Path.GetRandomFileName();

                    var concat = string.Concat(randomFileName, "@", DomainToTest);

                    rtn.Add(new VerificationDataRequest { EmailAddress = concat });
                }

                return rtn;
            }
        }

        /// <summary>
        /// Creates the and run work expect no errors.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task CreateAndRunWork_ExpectNoErrors()
        {
            // arrange
            var service = ApiClientFactory.Create();
            service.ProgressChanged += (o, args) => Console.WriteLine(JsonConvert.SerializeObject(args));

            // act
            var stopwatch = Stopwatch.StartNew();
            var verificationResponses = await service.ProcessAsync(new VerificationRequest { VerificationData = TestList1 }, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // assert
            ClassicAssert.True(verificationResponses != null);
            this.logger.LogInformation(JsonConvert.SerializeObject(verificationResponses));
            Console.WriteLine(
                JsonConvert.SerializeObject(verificationResponses));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);

            service.ProgressChanged -= (o, args) => Console.WriteLine(JsonConvert.SerializeObject(args));
        }

        /// <summary>
        /// Creates the and run performance test expect timings output only.
        /// </summary>
        [Test]
        public void CreateAndRunPerformanceTest_ExpectTimingsOutputOnly()
        {
            // arrange
            var service = ApiClientFactory.Create();
            service.ProgressChanged += (o, args) => Console.WriteLine(JsonConvert.SerializeObject(args));

            // act
            var stopwatch = Stopwatch.StartNew();
            var verificationResponses = service.ProcessAsync(
                new VerificationRequest { VerificationData = PerformanceTestList1 },
                CancellationToken.None).Result;
            stopwatch.Stop();

            // assert
            ClassicAssert.True(verificationResponses != null);
            this.logger.LogInformation(JsonConvert.SerializeObject(verificationResponses));
            Console.WriteLine(
                JsonConvert.SerializeObject(verificationResponses));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);

            service.ProgressChanged -= (o, args) => Console.WriteLine(JsonConvert.SerializeObject(args));
        }
    }
}