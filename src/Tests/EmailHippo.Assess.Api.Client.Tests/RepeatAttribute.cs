﻿// <copyright file="RepeatAttribute.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Tests
{
    using System;
    using System.Collections;
    using System.Linq;
    using NUnit.Framework;
    using NUnit.Framework.Interfaces;

    /// <summary>
    /// Repeat attribute.
    /// </summary>
    /// <seealso cref="DataAttribute" />
    public class RepeatAttribute : IParameterDataSource
    {
        private readonly int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepeatAttribute"/> class.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <exception cref="ArgumentOutOfRangeException">count - Repeat count must be greater than 0.</exception>
        public RepeatAttribute(int count)
        {
            if (count < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "Repeat count must be greater than 0.");
            }

            this.count = count;
        }

        /// <summary>
        /// Retrieves a list of arguments which can be passed to the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter of a parameterized test.</param>
        /// <returns>Data.</returns>
        public IEnumerable GetData(IParameterInfo parameter)
        {
            return Enumerable.Repeat(new object[0], this.count);
        }
    }
}