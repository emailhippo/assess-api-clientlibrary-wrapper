﻿// <copyright file="AssemblyInfo.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("EmailHippo.Assess.Api.Client.Tests")]