﻿// <copyright file="VerificationDataRequest.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Entities.Service.V1
{
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The verification data request.
    /// </summary>
    [ProtoContract]
    public sealed class VerificationDataRequest
    {
        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        [JsonProperty(Order = 1)]
        [ProtoMember(1)]
        [NotNull]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        [JsonProperty(Order = 2)]
        [ProtoMember(2)]
        [CanBeNull]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [JsonProperty(Order = 3)]
        [ProtoMember(3)]
        [CanBeNull]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [JsonProperty(Order = 4)]
        [ProtoMember(4)]
        [CanBeNull]
        public string LastName { get; set; }
    }
}