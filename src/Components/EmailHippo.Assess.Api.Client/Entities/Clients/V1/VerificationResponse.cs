﻿// <copyright file="VerificationResponse.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Entities.Clients.V1
{
    using EmailHippo.Assess.Api.Entities.V_1_0_0;
    using JetBrains.Annotations;
    using Newtonsoft.Json;

    /// <summary>
    /// The verification response.
    /// </summary>
    internal sealed class VerificationResponse
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        [JsonProperty(Order = 1)]
        [CanBeNull]
        public Result Result { get; set; }
    }
}