﻿// <copyright file="VerificationRequest.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Entities.Clients.V1
{
    using System.ComponentModel.DataAnnotations;
    using JetBrains.Annotations;
    using Newtonsoft.Json;

    /// <summary>
    /// The verification request.
    /// </summary>
    internal sealed class VerificationRequest
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [JsonProperty(Order = 1)]
        [MaxLength(255)]
        [EmailAddress]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        [JsonProperty(Order = 2)]
        [CanBeNull]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [JsonProperty(Order = 3)]
        [CanBeNull]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [JsonProperty(Order = 4)]
        [CanBeNull]
        public string LastName { get; set; }
    }
}