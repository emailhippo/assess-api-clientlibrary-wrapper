﻿// <copyright file="KeyAuthentication.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Logic.Configuration.V1
{
    using EmailHippo.Assess.Api.Client.Interfaces.Configuration;

    /// <summary>
    /// Key authentication.
    /// </summary>
    public sealed class KeyAuthentication : IConfiguration<Entities.Configuration.V1.KeyAuthentication>
    {
        /// <inheritdoc />
        public Entities.Configuration.V1.KeyAuthentication Get { get; set; }
    }
}