﻿// <copyright file="DefaultClient.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Client.Logic.Clients.V1
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using EmailHippo.Assess.Api.Client.Diagnostics.Common;
    using EmailHippo.Assess.Api.Client.Entities.Clients.V1;
    using EmailHippo.Assess.Api.Client.Entities.Configuration.V1;
    using EmailHippo.Assess.Api.Client.Helpers;
    using EmailHippo.Assess.Api.Client.Interfaces.Clients;
    using EmailHippo.Assess.Api.Client.Interfaces.Configuration;
    using EmailHippo.Assess.Api.Entities.V_1_0_0;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using ProtoBuf;

    /// <summary>
    /// The default client. Uses protobuf endpoint for speed and efficiency.
    /// </summary>
    internal sealed class DefaultClient : IClientProxy<VerificationRequest, VerificationResponse>
    {
        /// <summary>
        /// The API url.
        /// </summary>
        private const string ApiUrlFormat = @"https://api.hippoassess.com/v1/proto/{0}/{1}";

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger<DefaultClient> logger;

        /// <summary>
        /// The authentication configuration.
        /// </summary>
        [NotNull]
        private readonly IConfiguration<KeyAuthentication> authConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultClient" /> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="authConfiguration">The authentication configuration.</param>
        public DefaultClient(
            [NotNull] ILoggerFactory loggerFactory,
            [NotNull] IConfiguration<KeyAuthentication> authConfiguration)
        {
            this.logger = loggerFactory.CreateLogger<DefaultClient>();
            this.authConfiguration = authConfiguration;
        }

        /// <inheritdoc />
        public VerificationResponse Process(VerificationRequest request)
        {
            return this.ProcessAsync(request, CancellationToken.None).Result;
        }

        /// <inheritdoc />
        public async Task<VerificationResponse> ProcessAsync(Entities.Clients.V1.VerificationRequest request, CancellationToken cancellationToken)
        {
            if (this.logger.IsEnabled(LogLevel.Information))
            {
                this.logger.LogInformation((int)EventIds.MethodEnter, Messages.MethodEnter, @"ProcessAsync");
                this.logger.LogInformation((int)EventIds.ValidationProcessorRequest, Messages.ValidationProcessorRequest, this.authConfiguration.Get.LicenseKey, request.Email);
            }

            try
            {
                request.Validate();
            }
            catch (ValidationException exception)
            {
                this.logger.LogError((int)EventIds.Error, exception, Messages.ValidationError, string.Empty);
                throw;
            }

            string requestUrl = string.Format(ApiUrlFormat, this.authConfiguration.Get.LicenseKey, request.Email);

            if (!string.IsNullOrWhiteSpace(request.IpAddress))
            {
                requestUrl = $"{requestUrl}/{request.IpAddress}";
            }

            if (!string.IsNullOrWhiteSpace(request.FirstName) && !string.IsNullOrWhiteSpace(request.LastName))
            {
                requestUrl = $"{requestUrl}/{request.FirstName}/{request.LastName}";
            }
            else if (!string.IsNullOrWhiteSpace(request.FirstName))
            {
                requestUrl = $"{requestUrl}?=firstName={request.FirstName}";
            }
            else if (!string.IsNullOrWhiteSpace(request.LastName))
            {
                requestUrl = $"{requestUrl}?lastName={request.LastName}";
            }

            var stopwatch = Stopwatch.StartNew();

            Result deserializeResult = null;

            var response = await ClientGlobal.HttpClient.GetAsync(new Uri(requestUrl), cancellationToken).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using (var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                {
                    try
                    {
                        deserializeResult = Serializer.Deserialize<Result>(stream);
                    }
                    catch (Exception exception)
                    {
                        this.logger.LogError((int)EventIds.Error, exception, Messages.Error, "deserialization error");
                        throw;
                    }
                }
            }

            var responseResult = new VerificationResponse { Result = deserializeResult };

            stopwatch.Stop();

            if (!this.logger.IsEnabled(LogLevel.Information))
            {
                return responseResult;
            }

            this.logger.LogInformation((int)EventIds.TimerLogging, Messages.TimerLogging, "ProcessAsync", stopwatch.ElapsedMilliseconds);
            this.logger.LogInformation((int)EventIds.MethodEnter, Messages.MethodExit, @"ProcessAsync");

            return responseResult;
        }
    }
}